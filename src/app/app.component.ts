import { Component, OnInit } from '@angular/core';
import * as Rx from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  errors:any = {
  	'email': {
  		msg: '',
  		untouched: true
  	},
  	'passwd': {
  		msg: '',
  		untouched: true
  	},
  	'passwd2': {
  		msg: '',
  		untouched: true
  	}
  };

  ngOnInit() {
    Rx.Observable.merge(
    	Rx.Observable.fromEvent(document, 'keyup'),
    	Rx.Observable.fromEvent(document, 'change'),
    	Rx.Observable.fromEvent(document, 'submit'),
    	Rx.Observable.fromEvent(document, 'focusout')
    ).subscribe((evt:Event) => {

    	if ((evt.srcElement.id =='email') || (evt.srcElement.id =='passwd') || (evt.srcElement.id =='passwd2')) {

    		this.errors[evt.srcElement.id].untouched=false;

    		let inputElem:HTMLInputElement = <HTMLInputElement>evt.srcElement;
    		if (inputElem.checkValidity()) {

    			this.errors[evt.srcElement.id].msg='';

    			if ((evt.srcElement.id =='passwd') || (evt.srcElement.id=='passwd2')) {

    				if (inputElem.value.length<=4)
    					this.errors[evt.srcElement.id].msg='Длина пароля должна быть больше 4 символов';
    				else {
    					this.errors[evt.srcElement.id].msg='';
    					if (!this.errors['passwd2'].untouched && (
    							(<HTMLInputElement>document.getElementById('passwd')).value!=(<HTMLInputElement>document.getElementById('passwd2')).value)
    						)
    						this.errors['passwd2'].msg='Введенные пароли не совпадают';
    					else 
    						this.errors['passwd2'].msg='';
    				}
    			}
    		} 
    		else 
    			this.errors[evt.srcElement.id].msg=inputElem.validationMessage;
    	} else {
			if (evt.type=='submit') {
				alert('Форма заполнена правильно и может быть отправлена');
				evt.preventDefault();
			}
    	}

    });
  } 
}
